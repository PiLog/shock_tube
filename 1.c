// *************************************************************
// * Численное моделирование процесса течения в ударной трубе. *
// *  Рассматривается осесимметричное течение идеального газа. *
// * Базовая система дифференциальных уравнений: ур-ния Эйлера.*
// *     Используется явная классическая разностная схема      *
// *                      метода Давыдова                      *
// *************************************************************

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define N 100
#define N_D 101
#define M 20
#define M_D 21

double **dmatrix(int nrl, int nrh, int ncl, int nch)
{
  int i, nrow = nrh - nrl + 1, ncol = nch - ncl + 1;
  double **m;

  // Выделение памяти для массива указателей на строки
  m = (double **)malloc((size_t)((nrow + 1) * sizeof(double *)));
  if (!m)
    return NULL;

  // Выделение памяти для хранения элементов массива
  m += 1;   // Сдвиг указателя на 1 элемент для совместимости с индексацией в Си
  m -= nrl; // Сдвиг указателя на nrl элементов

  m[nrl] = (double *)malloc((size_t)((nrow * ncol + 1) * sizeof(double)));
  if (!m[nrl])
  {
    free((char *)(m + nrl - 1)); // Освобождение памяти для массива указателей на строки
    return NULL;
  }

  // Сдвиг указателя на 1 элемент для совместимости с индексацией в Си
  m[nrl] += 1;

  // Сдвиг указателей на столбцы
  for (i = nrl + 1; i <= nrh; i++)
  {
    m[i] = m[i - 1] + ncol;
  }

  // Возвращение указателя на первый элемент массива указателей на строки
  return m;
}

void free_dmatrix(double **m, int nrl, int nrh, int ncl, int nch)
{
  int i;

  // Освобождение памяти для элементов массива
  free((char *)(m[nrl] + ncl - 1));

  // Освобождение памяти для массива указателей на строки
  free((char *)(m + nrl - 1));
}

int main()
{

  // Описание используемых переменных.

  double R0, A0, RO0, DT, DX, DR, K,
      **RO, **U, **V, **P, **E, **RO1,
      **ROP, **ROB, **UP, **VB,
      **UE, **VE, **EE,
      **RU, **RUU, **RVU, **REU, **RV, **RUV, **RVV, **REV, **RM,
      A1, A2, A3, A4, A5, A6, A7, A8, A10;
  int I, J, NC;
  FILE *F01;

  // Определение указателей на переменные.

  RO = dmatrix(0, N_D, 0, M_D);
  U = dmatrix(0, N_D, 0, M_D);
  V = dmatrix(0, N_D, 0, M_D);
  P = dmatrix(0, N_D, 0, M_D);
  E = dmatrix(0, N_D, 0, M_D);
  RO1 = dmatrix(0, N_D, 0, M_D);
  ROP = dmatrix(0, N_D, 1, M_D);
  ROB = dmatrix(1, N_D, 0, M_D);
  UP = dmatrix(0, N_D, 1, M_D);
  VB = dmatrix(1, N_D, 0, M_D);
  UE = dmatrix(0, N_D, 0, M_D);
  VE = dmatrix(0, N_D, 0, M_D);
  EE = dmatrix(0, N_D, 0, M_D);
  RU = dmatrix(0, N_D, 0, M_D);
  RUU = dmatrix(0, N_D, 0, M_D);
  RVU = dmatrix(0, N_D, 0, M_D);
  REU = dmatrix(0, N_D, 0, M_D);
  RV = dmatrix(0, N_D, 0, M_D);
  RUV = dmatrix(0, N_D, 0, M_D);
  RVV = dmatrix(0, N_D, 0, M_D);
  REV = dmatrix(0, N_D, 0, M_D);
  RM = dmatrix(0, N_D, 0, M_D);

  // Открытие файла для печати расчётной информации.

  if ((F01 = fopen("/home/pilog/Documents/shock_tube/output/egorov.csv", "w")) == NULL)
  {
    printf("File F01 is not open!\n");
    return 1;
  }

  // Сообщение о начале счёта.

  printf("Mercury01, go,go,go!!!\n");
  // fprintf(F01, "Mercury01, go,go,go!!!\n");

  // Константы.

  R0 = 1;
  A0 = 340;
  RO0 = 1.204;
  DT = 4E-6 * A0 / R0;
  DX = 0.01 / R0;
  DR = 0.01 / R0;
  K = 1.4;

  // Начальные условия.

  for (I = 1; I <= N; I++)
  {
    for (J = 1; J <= M; J++)
    {
      if (I <= 20)
      {
        RO[I][J] = 20 * 1.204 / RO0;
        P[I][J] = 20 * 1.01325E5 / (RO0 * A0 * A0);
      }
      else
      {
        RO[I][J] = 1.204 / RO0;
        P[I][J] = 1.01325E5 / (RO0 * A0 * A0);
      }
      U[I][J] = 0.0;
      V[I][J] = 0.0;
      E[I][J] = P[I][J] / (RO[I][J] * (K - 1));
    }
  }

  //***************************************
  //*          Цикл по времени.           *
  //***************************************

  for (NC = 1; NC < 9000; NC++)
  {

    // ГУ: правая граница.

    for (J = 1; J <= M; J++)
    {
      RO[N + 1][J] = RO[N][J];
      U[N + 1][J] = -U[N][J];
      V[N + 1][J] = V[N][J];
      P[N + 1][J] = P[N][J];
    }

    // ГУ: нижняя граница - ось симметрии.

    for (I = 1; I <= N; I++)
    {
      RO[I][0] = RO[I][1];
      U[I][0] = U[I][1];
      V[I][0] = -V[I][1];
      P[I][0] = P[I][1];
    }

    // ГУ: левая граница.

    for (J = 1; J <= M; J++)
    {
      RO[0][J] = RO[1][J];
      U[0][J] = -U[1][J];
      V[0][J] = V[1][J];
      P[0][J] = P[1][J];
    }

    // ГУ: верхняя граница.

    for (I = 1; I <= N; I++)
    {
      RO[I][M + 1] = RO[I][M];
      U[I][M + 1] = U[I][M];
      V[I][M + 1] = -V[I][M];
      P[I][M + 1] = P[I][M];
    }

    //********************************
    //"Эйлеров" этап метода Давыдова.*
    //********************************

    // Вычисление "эйлеровых" параметров потока.

    for (I = 1; I <= N; I++)
    {
      for (J = 1; J <= M; J++)
      {
        A1 = (P[I][J] + P[I + 1][J]) / 2;
        A2 = (P[I - 1][J] + P[I][J]) / 2;
        A3 = (P[I][J] + P[I][J + 1]) / 2;
        A4 = (P[I][J - 1] + P[I][J]) / 2;
        UE[I][J] = U[I][J] - (A1 - A2) * DT / (RO[I][J] * DX);
        VE[I][J] = V[I][J] - (A3 - A4) * DT / (RO[I][J] * DR);
        A5 = (U[I][J] + U[I + 1][J]) / 2;
        A6 = (U[I - 1][J] + U[I][J]) / 2;
        A7 = (V[I][J] + V[I][J + 1]) / 2;
        A8 = (V[I][J - 1] + V[I][J]) / 2;
        EE[I][J] = E[I][J] - (A1 * A5 - A2 * A6) * DT / (RO[I][J] * DX) -
                   (J * A3 * A7 - (J - 1) * A4 * A8) * DT / (RO[I][J] * (J - 0.5) * DR);
      }
    }

    // Е.ГУ: правая граница.

    for (J = 1; J <= M; J++)
    {
      UE[N + 1][J] = -UE[N][J];
      VE[N + 1][J] = VE[N][J];
      EE[N + 1][J] = EE[N][J];
    }

    // Е.ГУ: нижняя граница - ось симметрии.

    for (I = 1; I <= N; I++)
    {
      UE[I][0] = UE[I][1];
      VE[I][0] = -VE[I][1];
      EE[I][0] = EE[I][1];
    }

    // Е.ГУ: левая граница.

    for (J = 1; J <= M; J++)
    {
      UE[0][J] = -UE[1][J];
      VE[0][J] = VE[1][J];
      EE[0][J] = EE[1][J];
    }

    // Е.ГУ: верхняя граница.

    for (I = 1; I <= N; I++)
    {
      UE[I][M + 1] = UE[I][M];
      VE[I][M + 1] = -VE[I][M];
      EE[I][M + 1] = EE[I][M];
    }

    // Анализ устойчивости вычислений.

    for (I = 1; I <= N; I++)
    {
      for (J = 1; J <= M; J++)
        if (P[I][J] < 0)
        {
          printf("Attention!!! P[I][J]= %e %d %d %d\n", P[I][J], I, J, NC);
          fprintf(F01, "Attention!!! P[I][J]= %e %d %d %d\n", P[I][J], I, J, NC);
          fprintf(F01, "     Parameter PR[I][J]=\n");
          for (I = 0; I <= N + 1; I++)
          {
            fprintf(F01, "%3d ", I);
            for (J = 0; J <= M + 1; J++)
            {
              RM[I][J] = P[I][J] * A0 * A0 * RO0 / 1E6;
              fprintf(F01, "%8.3f", RM[I][J]);
            }
            fprintf(F01, "\n");
          }
          fprintf(F01, "     Parameter U1R[I][J]=\n");
          for (I = 0; I <= N + 1; I++)
          {
            fprintf(F01, "%3d ", I);
            for (J = 0; J <= M + 1; J++)
            {
              RM[I][J] = U[I][J] * A0;
              fprintf(F01, "%8.2f", RM[I][J]);
            }
            fprintf(F01, "\n");
          }
          return 20;
        }
    }

    //**********************************
    //"Лагранжев" этап метода Давыдова.*
    //**********************************

    for (I = 0; I <= N; I++)
    {
      for (J = 0; J <= M; J++)
      {
        // Вдоль оси "0Х".

        A1 = (UE[I][J] + UE[I + 1][J]) / 2;
        if (A1 >= 0)
        {
          RU[I][J] = RO[I][J] * A1;
          RUU[I][J] = RU[I][J] * UE[I][J];
          RVU[I][J] = RU[I][J] * VE[I][J];
          REU[I][J] = RU[I][J] * EE[I][J];
        }
        else
        {
          RU[I][J] = RO[I + 1][J] * A1;
          RUU[I][J] = RU[I][J] * UE[I + 1][J];
          RVU[I][J] = RU[I][J] * VE[I + 1][J];
          REU[I][J] = RU[I][J] * EE[I + 1][J];
        }

        // Вдоль оси "0R".

        A1 = (VE[I][J] + VE[I][J + 1]) / 2;
        if (A1 >= 0)
        {
          RV[I][J] = RO[I][J] * A1;
          RUV[I][J] = RV[I][J] * UE[I][J];
          RVV[I][J] = RV[I][J] * VE[I][J];
          REV[I][J] = RV[I][J] * EE[I][J];
        }
        else
        {
          RV[I][J] = RO[I][J + 1] * A1;
          RUV[I][J] = RV[I][J] * UE[I][J + 1];
          RVV[I][J] = RV[I][J] * VE[I][J + 1];
          REV[I][J] = RV[I][J] * EE[I][J + 1];
        }
      }
    }

    //***************************************
    //"Заключительный" этап метода Давыдова.*
    //***************************************

    for (I = 1; I <= N; I++)
    {
      for (J = 1; J <= M; J++)
      {
        RO1[I][J] = RO[I][J] - (RU[I][J] - RU[I - 1][J]) * DT / DX -
                    (J * RV[I][J] - (J - 1) * RV[I][J - 1]) * DT / ((J - 0.5) * DR);
        A1 = DT / RO1[I][J];
        U[I][J] = RO[I][J] * UE[I][J] / RO1[I][J] -
                  (RUU[I][J] - RUU[I - 1][J]) * A1 / DX -
                  (J * RUV[I][J] - (J - 1) * RUV[I][J - 1]) * A1 / ((J - 0.5) * DR);
        V[I][J] = RO[I][J] * VE[I][J] / RO1[I][J] -
                  (RVU[I][J] - RVU[I - 1][J]) * A1 / DX -
                  (J * RVV[I][J] - (J - 1) * RVV[I][J - 1]) * A1 / ((J - 0.5) * DR);
        E[I][J] = RO[I][J] * EE[I][J] / RO1[I][J] -
                  (REU[I][J] - REU[I - 1][J]) * A1 / DX -
                  (J * REV[I][J] - (J - 1) * REV[I][J - 1]) * A1 / ((J - 0.5) * DR);
      }
    }

    // Дополнительные параметры.

    for (I = 1; I <= N; I++)
      for (J = 1; J <= M; J++)
        P[I][J] = (K - 1) * RO1[I][J] * (E[I][J] - (U[I][J] * U[I][J] + V[I][J] * V[I][J]) / 2);

    // Переприсвоение значений.

    for (I = 1; I <= N; I++)
      for (J = 1; J <= M; J++)
        RO[I][J] = RO1[I][J];

    // Печать результатов расчёта.

    // Печать текущая.

    if (!(NC % 25))
    {
      A10 = A0 * A0 * RO0 / 1E6;
      A1 = P[1][5] * A10;
      A2 = P[20][5] * A10;
      A3 = P[40][5] * A10;
      A4 = P[60][5] * A10;
      A5 = P[80][5] * A10;
      A6 = U[100][5] * A0;
      // fprintf(F01, "%5d %8.3f %8.3f %8.3f %8.3f %8.3f %8.2f\n",
      //         NC, A1, A2, A3, A4, A5, A6);
    }

    // Печать параметров по полному полю течения.

    // if (!(NC % 100) || NC == 1)
    // {
    //   fprintf(F01, "Parameters of stream %d\n", NC);
    //   fprintf(F01, "     Parameter PR[I][J]=\n");
    //   for (I = 1; I <= N; I++)
    //   {
    //     fprintf(F01, "%3d ", I);
    //     for (J = 1; J <= M; J++)
    //     {
    //       RM[I][J] = P[I][J] * A0 * A0 * RO0 / 1E6;
    //       fprintf(F01, "%8.3f", RM[I][J]);
    //     }
    //     fprintf(F01, "\n");
    //   }
    //   // fprintf(F01, "     Parameter UR[I][J]=\n");
    //   // for (I = 1; I <= N; I++)
    //   // {
    //   //   fprintf(F01, "%3d ", I);
    //   //   for (J = 1; J <= M; J++)
    //   //   {
    //   //     RM[I][J] = U[I][J] * A0;
    //   //     fprintf(F01, "%8.2f", RM[I][J]);
    //   //   }
    //   //   fprintf(F01, "\n");
    //   // }
    // }

    if (!(NC % 10) || NC == 1)
    {
      for (I = 1; I <= N; I++)
      {
        for (J = 1; J <= M; J++)
        {
          RM[I][J] = P[I][J] * A0 * A0 * RO0 / 1E6;
          if (J == M_D - 1)
          {
            fprintf(F01, "%8.3f", RM[I][J]);
            ;
            break;
          }
          fprintf(F01, "%8.3f,", RM[I][J]);
        }
        fprintf(F01, "\n");
      }
    }

    // if (!(NC % 100) || NC == 1)
    // {
    //   char pr_filename[20];                         // Имя файла для хранения данных PR
    //   char ur_filename[20];                         // Имя файла для хранения данных UR
    //   sprintf(pr_filename, "PR_stream_%d.txt", NC); // Формирование имени файла PR с учетом значения NC
    //   sprintf(ur_filename, "UR_stream_%d.txt", NC); // Формирование имени файла UR с учетом значения NC

    //   FILE *FPR = fopen(pr_filename, "w"); // Создание нового файла для PR
    //   FILE *FUR = fopen(ur_filename, "w"); // Создание нового файла для UR
    //   if (FPR == NULL || FUR == NULL)
    //   {
    //     printf("Ошибка при создании файлов.\n");
    //     return 1; // Выход из программы с ошибкой
    //   }

    //   // Запись данных PR
    //   for (I = 1; I <= N; I++)
    //   {
    //     for (J = 1; J <= M; J++)
    //     {
    //       RM[I][J] = P[I][J] * A0 * A0 * RO0 / 1E6;
    //       fprintf(FPR, "%8.3f", RM[I][J]);
    //     }
    //     fprintf(FPR, "\n");
    //   }

    //   // Запись данных UR
    //   for (I = 1; I <= N; I++)
    //   {
    //     for (J = 1; J <= M; J++)
    //     {
    //       RM[I][J] = U[I][J] * A0;
    //       fprintf(FUR, "%8.2f", RM[I][J]);
    //     }
    //     fprintf(FUR, "\n");
    //   }
    //   fclose(FPR); // Закрытие файла PR
    //   fclose(FUR); // Закрытие файла UR
    // }

    // if (NC == 1000)
    // {
    //   fclose(F01);
    //   printf("The END. Good time!\n");
    //   return 1000;
    // }
  } // Конец цикла по времени!

  // Освобождение динамической памяти для переменных.

  // free_dmatrix(RO, 0, N_D, 0, M_D);
  // free_dmatrix(U, 0, N_D, 0, M_D);
  // free_dmatrix(V, 0, N_D, 0, M_D);
  // free_dmatrix(P, 0, N_D, 0, M_D);
  // free_dmatrix(E, 0, N_D, 0, M_D);
  // free_dmatrix(RO1, 0, N_D, 0, M_D);
  // free_dmatrix(ROP, 0, N_D, 1, M_D);
  // free_dmatrix(ROB, 1, N_D, 0, M_D);
  // free_dmatrix(UP, 0, N_D, 1, M_D);
  // free_dmatrix(VB, 1, N_D, 0, M_D);
  // free_dmatrix(UE, 0, N_D, 0, M_D);
  // free_dmatrix(VE, 0, N_D, 0, M_D);
  // free_dmatrix(EE, 0, N_D, 0, M_D);
  // free_dmatrix(RU, 0, N_D, 0, M_D);
  // free_dmatrix(RUU, 0, N_D, 0, M_D);
  // free_dmatrix(RVU, 0, N_D, 0, M_D);
  // free_dmatrix(REU, 0, N_D, 0, M_D);
  // free_dmatrix(RV, 0, N_D, 0, M_D);
  // free_dmatrix(RUV, 0, N_D, 0, M_D);
  // free_dmatrix(RVV, 0, N_D, 0, M_D);
  // free_dmatrix(REV, 0, N_D, 0, M_D);
  return 0;
} // Конец Mercury01 !!!
