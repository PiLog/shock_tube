import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider
import imageio
import os

# Загрузка данных с помощью numpy
data = np.loadtxt('/home/pilog/Documents/shock_tube/output/temp.csv', delimiter=',')

# Получение размера каждого графика
chunk_size = 1000
num_plots = len(data) // chunk_size

# Создание начального графика
fig, ax = plt.subplots()
plt.subplots_adjust(bottom=0.25)  # Добавляем пространство под слайдер

# Создаем слайдер
ax_slider = plt.axes([0.25, 0.1, 0.65, 0.03], facecolor='lightgoldenrodyellow')
slider = Slider(ax_slider, 'Номер чанка', 0, num_plots - 1, valinit=0, valstep=1)

colorbar = None
levels = 500
print(np.min(data), np.max(data[1]))
mmin = 0
mmax = np.max(data[0])
# mmax = np.max(data)
# mmin = np.min(data)

arr = [0,10,60,75,140]
# 0, 10, 60, 75, 140

def update(val):    
    global colorbar
    if colorbar:
        colorbar.remove()
    chunk_idx = int(slider.val)
    start_idx = chunk_idx * chunk_size
    end_idx = start_idx + chunk_size
    chunk_data = data[start_idx:end_idx]
    
    mmin = np.min(chunk_data)
    mmax = np.max(chunk_data)
    ticks = np.linspace(mmin, mmax, num=20)
    level_boundaries = np.linspace(mmin, mmax, levels + 1)

    # Очищаем предыдущий график и строим новый
    ax.clear()

    x = np.arange(chunk_data.shape[1])
    y = np.arange(chunk_data.shape[0])
    X, Y = np.meshgrid(x, y)
    # cont = ax.contourf(X, Y, chunk_data, cmap='plasma', levels=100, ticks = ticks)  # Заливка контуров цветом
    cont = ax.contourf(X, Y, chunk_data, level_boundaries ,cmap='plasma', vmin=mmin, vmax=mmax)  # Заливка контуров цветом

    ax.set_xlabel('X координата')
    ax.set_ylabel('Y координата')
    ax.set_title(f'({start_idx + 1}-{end_idx})')

    plt.draw()

    colorbar = plt.colorbar(cont, ax=ax, ticks=ticks,
            boundaries=level_boundaries,
            values=(level_boundaries[:-1] + level_boundaries[1:]) / 2)
    # Добавляем новую цветовую шкалу
    # if colorbar is None:


if not input():
# if True:
    slider.on_changed(update)
    plt.show()
else:
    # Создаем гиф-анимацию
    for i in arr:
        slider.set_val(i)
        update(None)  # Вызываем обновление без события (val=None)
#        fig.canvas.draw()
#        plt.pause(0.01)  # Пауза для обновления графика
        filename = f'/home/pilog/Documents/shock_tube/output/frames/frame_{i:03d}.png'
        plt.savefig(filename)
        print(f"Сохранен кадр {i}")

    # Создаем список файлов PNG
    png_files = [f'/home/pilog/Documents/shock_tube/output/frames/frame_{i:03d}.png' for i in range(num_plots)]

    # # Считываем и объединяем кадры в GIF
    # images = [imageio.imread(file) for file in png_files]
    # imageio.mimsave('/home/pilog/Documents/shock_tube/output/temperature_animation.gif', images, fps=2)

    # input()
    # # Удаляем временные файлы PNG
    # for file in png_files:
    #     os.remove(file)
