#include <cmath>
#include "stdio.h"
#include <fstream>
#include <iostream>
#include <iomanip>

#define N 1000
#define N_D 1001
#define M 20
#define M_D 21
#define TIME 15000
#define IMAGE 100

int main()
{
    // Все обезразмерить

    printf("prog start\n");

    double **pressure = new double *[N_D];
    double **density = new double *[N_D];
    double **energy = new double *[N_D];
    double **energy_eul = new double *[N_D];
    double **velocity_u = new double *[N_D];
    double **velocity_v = new double *[N_D];
    double **velocity_u_eul = new double *[N_D];
    double **velocity_v_eul = new double *[N_D];
    double **gas_number = new double *[N_D];
    double **gas_number_lagr_u = new double *[N_D];
    double **gas_number_lagr_v = new double *[N_D];
    double **RU = new double *[N_D];
    double **RUU = new double *[N_D];
    double **RVU = new double *[N_D];
    double **REU = new double *[N_D];
    double **RV = new double *[N_D];
    double **RUV = new double *[N_D];
    double **RVV = new double *[N_D];
    double **REV = new double *[N_D];
    double **RO1 = new double *[N_D];
    double **RM = new double *[N_D];
    double **CP = new double *[N_D];
    double **CP_lagr_u = new double *[N_D];
    double **CP_lagr_v = new double *[N_D];
    double **T = new double *[N_D];

    for (int i = 0; i <= N_D; i++)
    {
        pressure[i] = new double[M_D];
        density[i] = new double[M_D];
        energy[i] = new double[M_D];
        energy_eul[i] = new double[M_D];
        velocity_u[i] = new double[M_D];
        velocity_v[i] = new double[M_D];
        velocity_u_eul[i] = new double[M_D];
        velocity_v_eul[i] = new double[M_D];
        gas_number[i] = new double[M_D];
        gas_number_lagr_u[i] = new double[M_D];
        gas_number_lagr_v[i] = new double[M_D];
        RU[i] = new double[M_D];
        RUU[i] = new double[M_D];
        RVU[i] = new double[M_D];
        REU[i] = new double[M_D];
        RV[i] = new double[M_D];
        RUV[i] = new double[M_D];
        RVV[i] = new double[M_D];
        REV[i] = new double[M_D];
        RO1[i] = new double[M_D];
        RM[i] = new double[M_D];
        CP[i] = new double[M_D];
        CP_lagr_u[i] = new double[M_D];
        CP_lagr_v[i] = new double[M_D];
        T[i] = new double[M_D];
    }

    double A1, A2, A3, A4, A5, A6, A7, A8;

    double densityHe = 0.178;
    double KHe = 1.67;
    double RHe = 2077;
    double THe = 500;
    double CvHe = RHe / (KHe - 1);
    double CpHe = RHe + CvHe;

    // double densityHe = 1.204;
    // double KHe = 1.4;
    // double RHe = 287;
    // double THe = 300;

    double densityAir = 1.204;
    double KAir = 1.4;
    double RAir = 287;
    double TAir = 300;
    double CvAir = RAir / (KAir - 1);
    double CpAir = RAir + CvAir;

    double density0 = densityAir;
    double K0 = KAir;
    double R0 = RAir;
    double T0 = TAir;
    double A0 = 340;

    double len0 = 1;
    double P0 = 1.01325E5;
    double A = pow(KHe * RHe * THe, 0.5); // Видмио и есть A
    double NU = 8;
    double DX = 0.005 / len0;
    double DR = 0.005 / len0;
    double DT = DX / (NU * A) * A0 / len0;

    // Начальные условия

    std::ofstream F("./summary.csv", std::ios::out);
    std::ofstream FE("./energy.csv", std::ios::out);
    std::ofstream FG("./gas.csv", std::ios::out);
    std::ofstream FCP("./capacity.csv", std::ios::out);
    std::ofstream FVU("./vel_u.csv", std::ios::out);
    std::ofstream FVV("./vel_v.csv", std::ios::out);
    std::ofstream FT("./temp.csv", std::ios::out);

    // F << "{";

    for (size_t i = 1; i < N_D; i++)
    {
        for (size_t j = 1; j < M_D; j++)
        {
            if (i <= 100)
            {
                pressure[i][j] = 50 * P0 / (density0 * A0 * A0);
                density[i][j] = pressure[i][j] / (RHe * THe) * (R0 * T0) / (density0);
                energy[i][j] = pressure[i][j] / (density[i][j] * (KHe - 1));
                gas_number[i][j] = KHe;
                CP[i][j] = CpHe;
            }
            else
            {
                pressure[i][j] = 1 * P0 / (density0 * A0 * A0);
                density[i][j] = pressure[i][j] / (RAir * TAir) * (R0 * T0) / (density0);
                energy[i][j] = pressure[i][j] / (density[i][j] * (KAir - 1));
                gas_number[i][j] = KAir;
                CP[i][j] = CpAir;
            }
            velocity_u[i][j] = 0.;
            velocity_v[i][j] = 0.;
            // if (i % 25 == 0 && j % 10 == 0)
            // {
            //     printf("Шаг %d %d ; p = %f, rho = %f, E = %f  \n", i, j, pressure[i][j], density[i][j], energy[i][j]);
            // }
        }
    }

    //***************************************
    //*          Цикл по времени.           *
    //***************************************

    for (size_t time = 1; time < TIME; time++)
    {
        // ГУ: правая граница.

        for (size_t j = 1; j < M_D; j++)
        {
            density[N + 1][j] = density[N][j];
            velocity_u[N + 1][j] = -velocity_u[N][j];
            velocity_v[N + 1][j] = velocity_v[N][j];
            pressure[N + 1][j] = pressure[N][j];
        }
        // ГУ: нижняя граница - ось симметрии.

        for (size_t i = 1; i < N_D; i++)
        {
            density[i][0] = density[i][1];
            velocity_u[i][0] = velocity_u[i][1];
            velocity_v[i][0] = -velocity_v[i][1];
            pressure[i][0] = pressure[i][1];
        }

        // ГУ: левая граница.

        for (size_t j = 1; j < M_D; j++)
        {
            density[0][j] = density[1][j];
            velocity_u[0][j] = -velocity_u[1][j];
            velocity_v[0][j] = velocity_v[1][j];
            pressure[0][j] = pressure[1][j];
        }

        // ГУ: верхняя граница.

        for (size_t i = 1; i < N_D; i++)
        {
            density[i][M_D] = density[i][M_D - 1];
            velocity_u[i][M_D] = velocity_u[i][M_D - 1];
            velocity_v[i][M_D] = -velocity_v[i][M_D - 1];
            pressure[i][M_D] = pressure[i][M_D - 1];
        }

        //********************************
        //"Эйлеров" этап метода Давыдова.*
        //********************************

        // Вычисление "эйлеровых" параметров потока.

        for (size_t i = 1; i < N_D; i++)
        {
            for (size_t j = 1; j < M_D; j++)
            {
                A1 = (pressure[i][j] + pressure[i + 1][j]) / 2; // Полусуммы давления(?)
                A2 = (pressure[i - 1][j] + pressure[i][j]) / 2;
                A3 = (pressure[i][j] + pressure[i][j + 1]) / 2;
                A4 = (pressure[i][j - 1] + pressure[i][j]) / 2;
                velocity_u_eul[i][j] = velocity_u[i][j] - (A1 - A2) * DT / (density[i][j] * DX);
                velocity_v_eul[i][j] = velocity_v[i][j] - (A3 - A4) * DT / (density[i][j] * DR);
                A5 = (velocity_u[i][j] + velocity_u[i + 1][j]) / 2;
                A6 = (velocity_u[i - 1][j] + velocity_u[i][j]) / 2;
                A7 = (velocity_v[i][j] + velocity_v[i][j + 1]) / 2;
                A8 = (velocity_v[i][j - 1] + velocity_v[i][j]) / 2;
                energy_eul[i][j] = energy[i][j] - (A1 * A5 - A2 * A6) * DT / (density[i][j] * DX) -
                                   (j * A3 * A7 - (j - 1) * A4 * A8) * DT / (density[i][j] * (j - 0.5) * DR);
            }
        }

        // Е.ГУ: правая граница.

        for (size_t j = 1; j < M_D; j++)
        {
            velocity_u_eul[N_D][j] = -velocity_u_eul[N_D - 1][j];
            velocity_v_eul[N_D][j] = velocity_v_eul[N_D - 1][j];
            energy_eul[N_D][j] = energy_eul[N_D - 1][j];
            gas_number[N_D][j] = gas_number[N_D - 1][j];
            CP[N_D][j] = CP[N_D - 1][j];
        }

        // Е.ГУ: нижняя граница - ось симметрии.

        for (size_t i = 1; i < N_D; i++)
        {
            velocity_u_eul[i][0] = velocity_u_eul[i][1];
            velocity_v_eul[i][0] = -velocity_v_eul[i][1];
            energy_eul[i][0] = energy_eul[i][1];
            gas_number[i][0] = gas_number[i][1];
            CP[i][0] = CP[i][1];
        }

        // Е.ГУ: левая граница.

        for (size_t j = 1; j < M_D; j++)
        {
            velocity_u_eul[0][j] = -velocity_u_eul[1][j];
            velocity_v_eul[0][j] = velocity_v_eul[1][j];
            energy_eul[0][j] = energy_eul[1][j];
            gas_number[0][j] = gas_number[1][j];
            CP[0][j] = CP[1][j];
        }

        // Е.ГУ: верхняя граница.

        for (size_t i = 1; i < N_D; i++)
        {
            velocity_u_eul[i][M_D] = velocity_u_eul[i][M_D - 1];
            velocity_v_eul[i][M_D] = -velocity_v_eul[i][M_D - 1];
            energy_eul[i][M_D] = energy_eul[i][M_D - 1];
            gas_number[i][M_D] = gas_number[i][M_D - 1];
            CP[i][M_D] = CP[i][M_D - 1];
        }

        //**********************************
        //"Лагранжев" этап метода Давыдова.*
        //**********************************

        for (size_t i = 0; i < N_D; i++)
        {
            for (size_t j = 0; j < M_D; j++)
            {
                // Вдоль оси "0Х".

                A1 = (velocity_u_eul[i][j] + velocity_u_eul[i + 1][j]) / 2;
                if (A1 >= 0)
                {
                    RU[i][j] = density[i][j] * A1;
                    RUU[i][j] = RU[i][j] * velocity_u_eul[i][j];
                    RVU[i][j] = RU[i][j] * velocity_v_eul[i][j];
                    REU[i][j] = RU[i][j] * energy_eul[i][j];
                    gas_number_lagr_u[i][j] = RU[i][j] * gas_number[i][j];
                    CP_lagr_u[i][j] = RU[i][j] * CP[i][j];
                }
                else
                {
                    RU[i][j] = density[i + 1][j] * A1;
                    RUU[i][j] = RU[i][j] * velocity_u_eul[i + 1][j];
                    RVU[i][j] = RU[i][j] * velocity_v_eul[i + 1][j];
                    REU[i][j] = RU[i][j] * energy_eul[i + 1][j];
                    gas_number_lagr_u[i][j] = RU[i][j] * gas_number[i + 1][j];
                    CP_lagr_u[i][j] = RU[i][j] * CP[i + 1][j];
                }

                // Вдоль оси "0R".

                A1 = (velocity_v_eul[i][j] + velocity_v_eul[i][j + 1]) / 2;
                if (A1 >= 0)
                {
                    RV[i][j] = density[i][j] * A1;
                    RUV[i][j] = RV[i][j] * velocity_u_eul[i][j];
                    RVV[i][j] = RV[i][j] * velocity_v_eul[i][j];
                    REV[i][j] = RV[i][j] * energy_eul[i][j];
                    gas_number_lagr_v[i][j] = RV[i][j] * gas_number[i][j];
                    CP_lagr_v[i][j] = RV[i][j] * CP[i][j];
                }
                else
                {
                    RV[i][j] = density[i][j + 1] * A1;
                    RUV[i][j] = RV[i][j] * velocity_u_eul[i][j + 1];
                    RVV[i][j] = RV[i][j] * velocity_v_eul[i][j + 1];
                    REV[i][j] = RV[i][j] * energy_eul[i][j + 1];
                    gas_number_lagr_v[i][j] = RV[i][j] * gas_number[i][j + 1];
                    CP_lagr_v[i][j] = RV[i][j] * CP[i][j + 1];
                }
            }
        }

        //***************************************
        //"Заключительный" этап метода Давыдова.*
        //***************************************

        for (size_t i = 1; i < N_D; i++)
        {
            for (size_t j = 1; j < M_D; j++)
            {
                //(double(*)[21]) RO1[100]
                RO1[i][j] = density[i][j] - (RU[i][j] - RU[i - 1][j]) * DT / DX -
                            (j * RV[i][j] - (j - 1) * RV[i][j - 1]) * DT / ((j - 0.5) * DR);

                A1 = DT / RO1[i][j];

                velocity_u[i][j] = density[i][j] * velocity_u_eul[i][j] / RO1[i][j] -
                                   (RUU[i][j] - RUU[i - 1][j]) * A1 / DX -
                                   (j * RUV[i][j] - (j - 1) * RUV[i][j - 1]) * A1 / ((j - 0.5) * DR);

                velocity_v[i][j] = density[i][j] * velocity_v_eul[i][j] / RO1[i][j] -
                                   (RVU[i][j] - RVU[i - 1][j]) * A1 / DX -
                                   (j * RVV[i][j] - (j - 1) * RVV[i][j - 1]) * A1 / ((j - 0.5) * DR);

                energy[i][j] = density[i][j] * energy_eul[i][j] / RO1[i][j] -
                               (REU[i][j] - REU[i - 1][j]) * A1 / DX -
                               (j * REV[i][j] - (j - 1) * REV[i][j - 1]) * A1 / ((j - 0.5) * DR);

                gas_number[i][j] = density[i][j] * gas_number[i][j] / RO1[i][j] - (gas_number_lagr_u[i][j] - gas_number_lagr_u[i - 1][j]) * A1 / DX -
                                   (j * gas_number_lagr_v[i][j] - (j - 1) * gas_number_lagr_v[i][j - 1]) * A1 / ((j - 0.5) * DR);

                CP[i][j] = density[i][j] * CP[i][j] / RO1[i][j] - (CP_lagr_u[i][j] - CP_lagr_u[i - 1][j]) * A1 / DX -
                           (j * CP_lagr_v[i][j] - (j - 1) * CP_lagr_v[i][j - 1]) * A1 / ((j - 0.5) * DR);
            }
        }

        // Дополнительные параметры.

        for (size_t i = 1; i < N_D; i++)
            for (size_t j = 1; j < M_D; j++)
            {
                pressure[i][j] = (gas_number[i][j] - 1) * RO1[i][j] *
                                 (energy[i][j] - (velocity_u[i][j] * velocity_u[i][j] + velocity_v[i][j] *
                                                                                            velocity_v[i][j]) /
                                                     2);
            }

        // Переприсвоение значений.

        for (size_t i = 1; i < N_D; i++)
            for (size_t j = 1; j < M_D; j++)
                density[i][j] = RO1[i][j];

        if (!(time % IMAGE) || time == 1)
        {

            if (!F && !FE)
            {
                std::cout << "Ошибка при создании файлов.\n";
                return 1;
            }
            for (size_t i = 1; i < N_D; i++)
            {
                for (size_t j = 1; j < M_D; j++)
                {
                    RM[i][j] = pressure[i][j] * (density[i][j] * A0 * A0) / 1E6;
                    F << std::setprecision(3) << (j == M_D - 1 ? std::to_string(RM[i][j]) : std::to_string(RM[i][j]) + ", ");

                    RM[i][j] = energy[i][j] * (density[i][j] * (KHe - 1));
                    FE << std::setprecision(3) << (j == M_D - 1 ? std::to_string(RM[i][j]) : std::to_string(RM[i][j]) + ", ");

                    RM[i][j] = gas_number[i][j];
                    FG << std::setprecision(3) << (j == M_D - 1 ? std::to_string(RM[i][j]) : std::to_string(RM[i][j]) + ", ");

                    RM[i][j] = CP[i][j];
                    FCP << std::setprecision(3) << (j == M_D - 1 ? std::to_string(RM[i][j]) : std::to_string(RM[i][j]) + ", ");

                    RM[i][j] = velocity_u[i][j];
                    FVU << std::setprecision(3) << (j == M_D - 1 ? std::to_string(RM[i][j]) : std::to_string(RM[i][j]) + ", ");

                    RM[i][j] = velocity_v[i][j];
                    FVV << std::setprecision(3) << (j == M_D - 1 ? std::to_string(RM[i][j]) : std::to_string(RM[i][j]) + ", ");

                    T[i][j] = (pressure[i][j]) / ((density[i][j] / (R0 * T0) 
                    * (density0)) * (CP[i][j] - CP[i][j] / gas_number[i][j] ));
                    FT << std::setprecision(3) << (j == M_D - 1 ? std::to_string(T[i][j]) : std::to_string(T[i][j]) + ", ");
                }
                // 10:00 ВТ, СР 11:10 417 кД
                // Постановка задачи, блок схема, метод, резы + вывод
                F << "\n";
                FE << "\n";
                FG << "\n";
                FCP << "\n";
                FVU << "\n";
                FVV << "\n";
                FT << "\n";
            }
            std::cout << "time = " << time << "\n";
        }
    }

    F.close();
    FE.close();
    FG.close();
    FCP.close();
    FVU.close();
    FVV.close();

    return 0;
}